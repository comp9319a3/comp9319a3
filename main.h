/*
 * Brandon Sandoval - z5020926
 * 17/04/15
 * COMP9319 - Assignment 3, a BWT search program
 */

#ifndef MAIN_H
#define MAIN_H

//Output debug messages if DEBUG defined
#ifdef DEBUG
#define DEBUG_OUT(x) cout << "Debug: " << x; //debug output with message
#define DEBUG_OUTs(x) cout << x; //debug output (silent) no message
#else
#define DEBUG_OUT(x)
#define DEBUG_OUTs(x)
#endif

struct Cstruct{
	char c;
	int val;
};

//Holds a range, used by backward_search() to return the range of matches in BWT
struct Range{
	int first;
	int last;
};
//TODO fix both these arrays so there ranges are [98] to save space, low priority
Cstruct C[128];
int clookup[256];
int cSize;

//These constants are used by read();
typedef int SELECT;
const SELECT BACK = 1; //Get previous CHAR at index (backward move)
const SELECT CUR = 2;  //Get current INDEX at index
const SELECT FORE = 3; //Get next CHAR at index (foreward move)

//How often in chars is the occ table indexed
const int spacing = 784;
//Max possible number of search term results per term.
//Spec says 3000, ill be generous and set it to 10000
//about 6-7 arrays will be this size at the same time
const int TERMS = 10000;


//Main function starting point
int main(int argc, char** argv);
//occ function, this function does not use the idx file and is slow!
int occOld(char c, int p, int q, char* bwtName, bool keepOpen);
//occ function, this function walks to the nearest checkpoint by using occ_old
//and then reads the idx file to speed up the process.
int occ(char c, int q, char* bwtName);
//This function constructs the idx file, at every 784th char, an array of size 
//4x98bytes is inserted into the file. This array is the precomputed occ() table
void construct_idx(char* filename, char* idxName);
//This function finds character c at position num in the idx file and returns 
//its value, i.e. the occurance of that char at a fixed interval.
//Note1: read_idx will not release the idx file if keepOpen==true,
//we will only ever use keepOpen=false if we want to use the idx file outside of
//this function like using construct_idx again or when closing the program.
//Note2: File name is allowed to be NULL if read_idx has been called atleast
//once and the file has been keeped open, also note that construct_idx will call
//the read_idx() once so read_idx will be ready after construct_idx()
int read_idx(char c, int num, char* idxName, bool keepOpen);
//The FM-Index backward search algorithm on BWT files
Range backward_search(char* P, char* bwtName, int filesize);
//read function will get the index or char at the current, previous or next 
//location, see SELECT constants, keepOpen works same way as read_idx and occOld
int read(int index, char* bwtName, SELECT move, bool keepOpen);
//AND together 2 arrays, a1 and a2, modifies a1 to hold answer, 
//sizes must be given, max size each array 3000 elements
void andArray(int a1[], int* a1size, int a2[], int a2size);
//Unique a sorted array, max size 3000 elements
void uniqueArray(int array[], int* size);
//Sort a list of offsets and move results in sync
void offsetSort(int* offsets, int* size, int* records);

#endif //MAIN_H

