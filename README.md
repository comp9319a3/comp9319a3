Assignment 3 for COMP9319 at UNSW

A text data search program to search BWT encoded files.
See specifications at: http://www.cse.unsw.edu.au/~wong/cs9319-15a3.html
This Repository is located at https://bitbucket.org/comp9319a3/comp9319a3

This is a PRIVATE assignment. Only the owner should have access to these files. 
Brandon Sandoval.