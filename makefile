#
# Brandon Sandoval - z5020926
# 17/04/15
# Makefile for COMP9319 Assignment 3
#

# "make" 					to make program normally
# "make DEBUG=1"			to make program output debug files and messages
# "make clean"				to clean up object, program and .idx files

PROGRAMNAME=bwtsearch
COMPILER=g++
CFLAGS=-std=c++11 -O3 -flto
ifeq ($(DEBUG),1)
CFLAGS+=-DDEBUG
else
CFLAGS+=-DNDEBUG
endif

$(PROGRAMNAME): main.o
	$(COMPILER) main.o $(CFLAGS) -o $(PROGRAMNAME)

main.o: main.cpp
	$(COMPILER) main.cpp -c $(CFLAGS) $(MEMSIZE)
clean:
	rm *.o *.idx $(PROGRAMNAME)

