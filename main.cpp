/*
 * Brandon Sandoval - z5020926
 * 17/04/15
 * COMP9319 - Hi Raymond, This is my Assignment 3, a BWT search program
 * This program will use 7.598MB on wagner, it is always this size since
 * everything is predictable and uses fixed width buffers.
 */

#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <assert.h>
#include <cstdlib>
#include "main.h"
using namespace std;

int main(int argc, char** argv){
	int freq[128] = {0};
	ifstream inputFile;
	inputFile.open(argv[1], ios::in|ios::binary|ios::ate);

	if(inputFile == NULL){
		cerr << "Error: File does not exist!" << endl;
		exit(1);
	}
	//number of patterns
	int patterns = argc-4;
	if((argc <= 4) || (argc >= 10)){
		cerr << "Error: too many/few arguments!" << endl;
		exit(1);
	}
	
	int filesize = inputFile.tellg();
	int readAmount = 0;
	const int DATASIZE = 4700000; //~5MB reads each time to build freq[]
	char* datablock = new char[DATASIZE];
	inputFile.seekg(0);
	
	while(filesize >= readAmount){
		if(filesize >= readAmount+DATASIZE){
			inputFile.read(datablock, DATASIZE);
			for(int i = 0; i < DATASIZE; i++){
				freq[datablock[i]]++;
			}
		}else{
			inputFile.read(datablock, filesize-readAmount);
			for(int i = 0; i < filesize-readAmount; i++){
				freq[datablock[i]]++;
			}
			break;
		}
		readAmount += DATASIZE;
	}
	//We built the freq[] each time, as idx holds almost 50% of bwt occ
	//building freq is really fast anyway, and we delete the data buffer now
	delete[] datablock;

#ifdef DEBUG
	//Output the freq[] and C[] to debug file to check if its working 
	ofstream outs;
	outs.open("debug.txt");
#endif
	int sum = 0, sumprev = 0, count = 0;
	for(int i = 0; i < 127; i++){
#ifdef DEBUG
		outs << "freq[" << i << "]=" << freq[i] << endl;
#endif
		sum += freq[i];
		if(sum > sumprev){
			C[count].c = i;
			C[count].val = sumprev;
			count++;
		}
		sumprev = sum;
	}

	cSize = count;
	for(int i = 0; i < cSize; i++){
#ifdef DEBUG
		outs << "C[" << i << "]= '" << C[i].c << "', " 
			<< C[i].val << endl;
#endif
		clookup[C[i].c] = i;
	}
#ifdef DEBUG
	outs.close();
#endif

	construct_idx(argv[1], argv[2]);
	DEBUG_OUT("----finished construction of idx----" << endl);
	
	Range range[5];
	int** results = new int*[5];
	int result_size[5] = {0};
	for(int i = 0; i < 5; i++){
		results[i] = new int[TERMS];
	}
	
	int* resultsTemp = new int[TERMS];
	int resultsTemp_size = 0;

	for(int p = 0; p < patterns; p++){
		range[p] = backward_search(argv[4+p], argv[1], filesize);
		//cout << "done" << endl;
		for(int i = 0; i < range[p].last-range[p].first+1; i++){
			int r = range[p].first+i-1;
			char c = read(r, argv[1], CUR, true);
		
			while(c != ']'){
				r = read(r, argv[1], BACK, true);
				c = read(r, argv[1], CUR, true);
			}
		
			results[p][result_size[p]] = r;
			result_size[p]++;
		}
	}
	DEBUG_OUT("s1: " << result_size[0] << endl);
	DEBUG_OUT("s2: " << result_size[1] << endl);
	DEBUG_OUT("s3: " << result_size[2] << endl);
	DEBUG_OUT("s4: " << result_size[3] << endl);
	DEBUG_OUT("s5: " << result_size[4] << endl);
	
	//This part uses 10000*6*4 bytes, but this is ok, as we used 5MB before
	//and deleted it, so we have heaps of room to play with.
	
	if(patterns >= 2){ //join 1 and 2
		andArray(results[0], &(result_size[0]), results[1], result_size[1]);
	}
	if(patterns >= 3){ //join 1 and 3
		andArray(results[0], &(result_size[0]), results[2], result_size[2]);
	}
	if(patterns >= 4){ //join 1 and 4
		andArray(results[0], &(result_size[0]), results[3], result_size[3]);
	}
	if(patterns == 5){ //join 1 and 5
		andArray(results[0], &(result_size[0]), results[4], result_size[4]);
	}
	sort(results[0], results[0]+result_size[0]);
	uniqueArray(results[0], &result_size[0]);
	
	//RETRIVE OFFSET
	
	int offsets[TERMS] = {0};
	int offset_size = 0;
	char number[25] = {0}; //get offset number and change it to int.
	int r;
	char c;
	
	for(int i = 0; i < result_size[0]; i++){
		r = read(results[0][i], argv[1], BACK, true);
		char c = 0;
		count = 0;
		while(c != '['){
			number[count] = (char)read(r, argv[1], CUR, true);
			r = read(r, argv[1], BACK, true);
			c = read(r, argv[1], CUR, true);
			count++;
		}
		number[count] = '\0';
		//Reverse the backwards read text and store it as int offset
		reverse(begin(number), begin(number)+count);
		//cout << "num:" << number << endl;
	
		offsets[offset_size] = atoi(number);
		offset_size++;
	}
	offsetSort(offsets, &offset_size, results[0]);
	
	//IMPLEMENT FORWARD SEARCH HERE
	int index = 0;
	//Run for the number of records in file, or parameter given, whichever is smallest
	int run = min(result_size[0], atoi(argv[3]));
	for(int i = 0; i < run; i++){
		c = '\0';
		index = read(results[0][i], argv[1], FORE, true);
		c = read(index, argv[1], CUR, true);
		cout << "[" << offsets[i] << "]";
		while(c != '['){
			cout << c;
			index = read(index, argv[1], FORE, true);
			c = read(index, argv[1], CUR, true);
		}
		cout << endl;
	}

	//Clean up
	for(int i = 0; i < 5; i++)
		delete[] results[i];
	delete[] resultsTemp;
	delete[] results;
	read_idx(0, 0, NULL, false);  //tell read_idx to release memory and idxFile
	occOld(0, 0, 0, NULL, false); //tell occOld to release memory and bwtfile
	read(0, NULL, 0, false);  //tell read to release memory and bwtfile
	return 0;
}

void offsetSort(int* offsets, int* size, int* records){
	//Bubble sort the final list for displaying
	//Slow but list must be <3000 elements as per spec
	int swap1, swap2;
	for(int i = 0; i < (*size)-1; i++){
		for(int j = i+1; j < *size; j++){
			if(offsets[i] > offsets[j]){
				swap1 = offsets[i];
				swap2 = records[i];
				offsets[i] = offsets[j];
				records[i] = records[j];
				offsets[j] = swap1;
				records[j] = swap2;
			}
		}
	}
}
//Warning, the next 2 functions will use an additional 10000*4 bytes to hold a temp array 
void uniqueArray(int array[], int* size){
	if(*size <= 1){
		return;
	}
	int* temp = new int[TERMS];
	int tempSize = 0;
	int prev = -1;
	
	for(int i = 0; i < *size; i++){
		if(prev != array[i]){
			temp[tempSize] = array[i];
			tempSize++;
		}
		prev = array[i];
		
	}
	
	memcpy(array, temp, tempSize*sizeof(int));
	*size = tempSize;
	delete[] temp;
}

void andArray(int a1[], int* a1size, int a2[], int a2size){
	int* temp = new int[TERMS];
	int tempSize = 0;
	for(int i = 0; i < *a1size; i++){
		for(int j = 0; j < a2size; j++){
			if(a1[i] == a2[j]){
				temp[tempSize] = a1[i];
				tempSize++;
			}
		}
	}
	memcpy(a1, temp, tempSize*sizeof(int));
	*a1size = tempSize;
	delete[] temp;
}

int occOld(char c, int p, int q, char* bwtName, bool keepOpen){
	//Warning occOld will keep the file open until keekOpen = false
	//function never writes to file
	//This is to keep faster file accessing without having to reopen
	
	static ifstream bwtFile;
	static int filesize;
	static bool openOnce = false;
	static char* datablock;
	
	if((openOnce == true) && (keepOpen == false)){ //release the file and return
		bwtFile.close();
		delete[] datablock;
		openOnce = false;
		return 0;
	}
	
	//Open the file once
	if(openOnce == false){ 
		bwtFile.open(bwtName, ios::in|ios::binary|ios::ate);
		filesize = bwtFile.tellg();
#ifdef DEBUG
		datablock = new char[filesize];
#else
		datablock = new char[spacing];
#endif
		if(bwtFile == NULL){
			cerr << "occOld() cannot open bwt file" << endl;
			exit(1);
		}
		openOnce = true;
	}
	bwtFile.seekg(p);
	bwtFile.read(datablock, q-p);
	
	int count = 0;
	for(int i = 0; i < q-p; i++){
		if(datablock[i] == c){
			count++;
		}
	}
	return count;
}

int occ(char c, int q, char* bwtName){
	//Set the offset for prefix for occOld so the whole BWT
	//does not need to be searched
	double p = q/spacing;
	int r_num = round(p);
	int f_num = floor(p);
	//We rounded down, therefore add the remainding occOld
	
	if(f_num == r_num){
		if(r_num > 0)
			return read_idx(c, r_num-1, NULL, true) + occOld(c, r_num*spacing, q, bwtName, true);
		else
			//r_num is 0, we dont need to look at idx
			return occOld(c, r_num*spacing, q, bwtName, true);
	}
	//We rounded up, therefore subtract the extra occOld
	else if(f_num != r_num){ //i.e. ceil == round 
		return read_idx(c, r_num-1, NULL, true) - occOld(c, q, r_num*spacing, bwtName, true);
	}
}

void construct_idx(char* filename, char* idxName){
//This code checks to see if idx already exists and then skips the construction
//however for testing purposes we will always reconstruct the idx file

	//check to see if the idx already exists
	ifstream idxFileInput;
	idxFileInput.open(idxName, ios::in|ios::binary);
	if(idxFileInput == NULL){
		DEBUG_OUT("idx file does not exist, it will be build" << endl);
		idxFileInput.close();
	}else{
		//Early return, because the file already exists and we do not need to
		//construct it
		DEBUG_OUT("using existing idx file" << endl);
		idxFileInput.close();
		read_idx(0, 0, idxName, true); 
		return;
	}	

	ofstream idxFileOutput; //.IDX FILE
	idxFileOutput.open(idxName, ios::out|ios::binary);
	
	ifstream inputFile; //BWT FILE
	inputFile.open(filename, ios::in|ios::binary|ios::ate);
	if(!inputFile){
		cerr << "cannot open file" << endl;
		exit(1);
	}
	//int blocksize = 1200;
	int blocksize = 6000; //~5MB to build idx file
	//we already deleted the 5MB that we used to build freq[] with, so this is ok
	char* datablock = new char[spacing*blocksize];
	int FILE_HEAD = 0;
	int FILE_TAIL = 0;
	int filesize = inputFile.tellg();
	inputFile.seekg(0);
	
	if(filesize <= spacing*blocksize){
		FILE_TAIL = filesize;
		inputFile.read(datablock, filesize);
	}else{
		FILE_TAIL = spacing*blocksize;
		inputFile.read(datablock, FILE_TAIL);
	}
	
	int count = 0;
	int readAmount = 0;
	int prevReadAmount = 0;
	//32-126,9,10,13 <- ACSII in respect to
	//0-94,97,95,96<- array indexes
	const int arraysize=98;
	int array[arraysize] = {0};
	bool lastRun = false;
	DEBUG_OUT("Filesize: " << filesize << endl);
	while(readAmount < filesize){
		//Stop writing to idx file
		if(readAmount+spacing > filesize){
			readAmount += filesize-readAmount;
			DEBUG_OUT(prevReadAmount << " - " << readAmount << endl);
			break;
		}else{
			//Read left over bytes in T
			readAmount += spacing;
		}
		DEBUG_OUT(prevReadAmount << " - " << readAmount << endl);
		
		//Update the file index positions
		if(readAmount > FILE_TAIL){
			FILE_HEAD = FILE_TAIL;
			if(FILE_TAIL+(spacing*blocksize) > filesize){
				FILE_TAIL = filesize;
			}else{
				FILE_TAIL = (FILE_TAIL+(spacing*blocksize));
			}
			inputFile.read(datablock, FILE_TAIL-FILE_HEAD);
		}
		//Prepare data into a fixed array size 98*4byte
		for(int i = prevReadAmount-FILE_HEAD; i < readAmount-FILE_HEAD; i++){
			assert((i >= 0));
			assert(i <= spacing*blocksize);
			if((datablock[i] >= 32) && (datablock[i] <= 126)){
				array[datablock[i]-32]++;
			}else if(datablock[i] == 10){
				array[95]++;
			}else if(datablock[i] == 13){
				array[96]++;
			}else if(datablock[i] == 9){
				array[97]++;
			}else{
				cerr << "Error: Invalid character in BWT file!" << endl;
				exit(1);
			}
		}
		prevReadAmount = readAmount;
		if(!lastRun){
			count++;
			idxFileOutput.write((char*)&array, 
			sizeof array);
		}
	}
	DEBUG_OUT("# of times we wrote to file = " << count << endl);
	inputFile.close();
	idxFileOutput.close();
	//construct_idx finishes with the idx file opened in the read_idx()
	delete[] datablock;
	read_idx(0, 0, idxName, true); 
}

int read_idx(char c, int rNum, char* idxName, bool keepOpen){
	//Warning read_idx will keep the idx file open until keekOpen = false
	//but this is ok as read_idx never write to this file, just remember to close it
	static int counter = 0;
	static ifstream idxFile;
	static bool openOnce = false;
	
	if(keepOpen == false){ //release the file and return
		idxFile.close();
		openOnce = false;
		return 0;
	}
	
	//Open the file once
	if(openOnce == false){ 
		idxFile.open(idxName, ios::in|ios::binary);
		if(idxFile == NULL){
			cerr << "read_idx() cannot open idx file" << endl;
			exit(1);
		}
		openOnce = true;
	}
	
	if(rNum < 0){
		return 0;
	}
	
	//We seek to the part where array is in File and read in the array, we could
	//improve this so we also jump to the correct position in the array 
	//and read 1 char instead of 1 array.
	int seek = rNum*98*4;
	idxFile.seekg(seek);
	//Grab array content	
	int array[98];
	idxFile.read((char*)&array, sizeof array);
	//return its value
	if(c == 10){
		return array[95];
	}else if(c == 13){
		return array[96];
	}else if(c == 9){
		return array[97];
	}else if(c == 0){
		return 0;
	}
	
	assert( (c >= 32) && (c <= 126) ); //Otherwise out-of-bounds
	return array[c-32];	
}

Range backward_search(char* P, char* bwtName, int filesize){
	int p = strlen(P)-1;
	int i = p;
	char c = P[p];
	Range range;
	int lookup = clookup[c];
	range.first = C[lookup].val+1;
	if(lookup+1 >= cSize)
		//Special case (last element, so no C[l+1] value
		range.last = C[lookup].val + occ(c, filesize, bwtName); 
	else
		range.last = C[lookup+1].val;
	
	DEBUG_OUT(i << endl);
	DEBUG_OUT("c:" << c << endl);
	DEBUG_OUT("first:" << range.first << endl);
	DEBUG_OUT("last:" << range.last << endl);
	DEBUG_OUT("l-f+1:" << range.last - range.first + 1 << endl);
	DEBUG_OUT("----" << endl);

	while((range.first<=range.last) && (i>=1)){
		c = P[i-1];
		
		//IMPROVE THIS TO USE NEW OCC
		assert(occOld(c, 0, range.first-1, bwtName, true) == occ(c, range.first-1, bwtName));
		assert(occOld(c, 0, range.last, bwtName, true) == occ(c, range.last, bwtName));
		
		range.first = C[clookup[c]].val 
			+ occ(c, range.first-1, bwtName)+1;
		range.last = C[clookup[c]].val 
			+ occ(c, range.last, bwtName);
			
		i--;
		DEBUG_OUT(i << endl);
		DEBUG_OUT("c:" << c << endl);
		DEBUG_OUT("first:" << range.first << endl);
		DEBUG_OUT("last:" << range.last << endl);
		DEBUG_OUT("l-f+1:" << range.last - range.first + 1 << endl);
		DEBUG_OUT("----" << endl);
	}
	if(range.last<range.first){
		DEBUG_OUT("NO MATCHES" << endl);
	}else{
		DEBUG_OUT("# OF MATCHES = " << range.last-range.first+1 << endl);
	}
	//cout << "matches" << range.last-range.first+1 << endl;
	return range;
}

int read(int index, char* bwtName, SELECT move, bool keepOpen){
	//Warning read will keep the file open until keekOpen = false
	//but this is ok as function will never write to file.
	//This is to keep faster file accessing without having to reopen
	
	static ifstream bwtFile;
	static int fileSize = 0;
	static bool openOnce = false;
	static char* datablock;
	
	if((openOnce == true) && (keepOpen == false)){ //release the file and return
		bwtFile.close();
		delete[] datablock;
		openOnce = false;
		return 0;
	}
	
	//Open the file once
	if(openOnce == false){ 
		bwtFile.open(bwtName, ios::in|ios::binary|ios::ate);
		fileSize = bwtFile.tellg();
		datablock = new char[1];
		if(bwtFile == NULL){
			cerr << "occOld() cannot open bwt file" << endl;
			exit(1);
		}
		openOnce = true;
	}
	//Get 1 char at index
	assert((index >= 0) && (index < fileSize));
	//DO NOT ATTEMPT TO SEEK OUTSIDE RANGE or stream will quietly break
	bwtFile.seekg(index);
	bwtFile.read(datablock, 1);
	char n = datablock[0];
	if(move == BACK){
		assert(occOld(n, 0, index, bwtName, true) == occ(n, index, bwtName));
		return C[clookup[n]].val + occ(n, index, bwtName);
	}else if(move == CUR){
		return n;
	}else if(move == FORE){
		//Foreward read is a binary search on bwtFile
		char nextChar;
		int count = cSize-1;
		//Lazy approach, linear scan over C[] instead of binary
		//C[] is usally small, max size is ~100 due to alphabeta size
		while(C[count].val > index){
			count--;
		}
		nextChar = C[count].c;
		
		int goal = index+1 - C[count].val;
		int nextIndex = 0;
		
		//Here we must do a Binary search in BWT file when forward searching
		int high = fileSize;
		int low = 0;
		int compute, result, mid;
		while(high>=low){
			mid = (high+low)/2;
			compute = occ(nextChar, mid, bwtName);
			if(compute == goal){
				result = mid;
				high = mid-1;
			}else if(compute < goal){
				low = mid+1;
			}else{
				high = mid-1;
			}
		}
		return result-1;
	}
}

